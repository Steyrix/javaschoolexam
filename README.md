# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Georgiy Pevnev (RU: Георгий Певнев)
* Codeship : [ ![Codeship Status for Steyrix/javaschoolexam](https://app.codeship.com/projects/30321f20-4be2-0136-984f-5e7b9ce4014a/status?branch=master)](https://app.codeship.com/projects/293088)
