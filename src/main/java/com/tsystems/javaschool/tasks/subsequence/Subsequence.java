package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if(x == null || y == null)
            throw new IllegalArgumentException("Argument is invalid (cannot be null)");

        if(x.equals(y))
            return true;

        int xPointer = 0;
        for(Object obj : y){

            if(xPointer == x.size())
                return true;

            if(obj.equals(x.get(xPointer)))
                xPointer++;
        }

        return false;
    }
}
