package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    private final int MAX_THRESHOLD = 1000000000;

    private void fillPyramid(int[][] target, List<Integer> src, int levelCount, int rowLength) {

        boolean[][] assigned = new boolean[levelCount][rowLength];
        int pointer = 0;

        for (int i = 0; i < levelCount; i++) {
            for (int j = 0; j < rowLength; j++) {
                if (i == 0) {
                    if (j == rowLength / 2) {
                        target[i][j] = src.get(pointer++);
                        assigned[i][j] = true;
                    }
                } else if (((j < rowLength - 1 && assigned[i - 1][j + 1]) ||
                        (j > 0 && assigned[i - 1][j - 1]))) {
                    target[i][j] = src.get(pointer++);
                    assigned[i][j] = true;
                }
            }
        }
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers == null || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        //In case of input data can cause running out of memory
        if (inputNumbers.size() >= MAX_THRESHOLD)
            throw new CannotBuildPyramidException();

        inputNumbers.sort(Comparator.naturalOrder());

        int levelCount = 0;
        int elemCount = 0;

        while (elemCount < inputNumbers.size())
            elemCount += levelCount++ + 1;

        if (elemCount != inputNumbers.size()) throw new CannotBuildPyramidException();

        final int rowLength = levelCount * 2 - 1;
        int[][] out = new int[levelCount][rowLength];

        fillPyramid(out, inputNumbers, levelCount, rowLength);

        return out;

    }
}
