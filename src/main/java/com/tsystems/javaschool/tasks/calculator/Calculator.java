package com.tsystems.javaschool.tasks.calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Calculator {

    @FunctionalInterface
    private interface operation {
        Double executeOperation(double r, double l);
    }

    private Map<Character, operation> opsExecute = new HashMap<>();

    public Calculator() {
        opsExecute.put('+', (r, l) -> r + l);
        opsExecute.put('-', (r, l) -> r - l);
        opsExecute.put('*', (r, l) -> r * l);
        opsExecute.put('/', (r, l) -> {
            if (l == 0)
                return null;
            else return r / l;
        });
        opsExecute.put('%', (r, l) -> r % l);
    }

    //Removes trailing zeros if there are ones
    private String formatAnswer(String rawAns){
        if(rawAns.isEmpty()) return null;

        return !rawAns.contains(".") ? rawAns :
                rawAns.replaceAll("0*$","").
                        replaceAll("\\.$", "");
    }

    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    private boolean isUnary(char c) {
        return c == '-';
    }

    private int getPriority(char op) {
        if (op == 'm')
            return 3;

        return op == '+' || op == '-' ? 1 :
                op == '*' || op == '/' || op == '%' ? 2 : -1;
    }

    private boolean isDelimiter(char c) {
        return Character.isWhitespace(c);
    }

    private void executeOperator(Stack<Double> operands, char op) {
        if (getPriority(op) > 2) {
            double l = operands.pop();
            operands.push(-l);
        } else {
            double r = operands.pop();
            double l = operands.pop();
            operands.push(opsExecute.get(op).executeOperation(l,r));
        }
    }

    //Defines whether there are operators of higher priority to execute before current operator
    private boolean thereIsToExecute(Stack<Character> operators, char c) {
        return !operators.empty() &&
                ((getPriority(operators.peek()) >= getPriority(c) && !isUnary(c)) ||
                        (getPriority(operators.peek()) > getPriority(c)) && isUnary(c));
    }

    public String processExpression(String expr) throws Exception {
        if (expr == null || expr.isEmpty()) return null;
        Stack<Double> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();
        boolean mayUnary = true;
        char previous = ' ';

        for (int i = 0; i < expr.length(); i++) {
            char c = expr.toCharArray()[i];
            if (!isDelimiter(c)) {
                if (c == '(') {
                    operators.push('(');
                    mayUnary = true;
                    previous = c;
                } else if (c == ')') {
                    while (operators.peek() != '(')
                        executeOperator(operands, operators.pop());

                    operators.pop();
                    mayUnary = false;
                    previous = c;
                } else if (isOperator(c)) {
                    if(c == previous || (isOperator(previous) && !isUnary(c))){
                        throw new Exception("c: " + c + ". prev: " + previous);
                    }

                    if (mayUnary && isUnary(c))
                        c = 'm';

                    while (thereIsToExecute(operators, c))
                        executeOperator(operands, operators.pop());

                    operators.push(c);
                    mayUnary = true;
                    previous = c;
                } else { //Parse number
                    previous = c;
                    String number = "";
                    int dotCounter = 0;
                    while ((Character.isDigit(c) || c == '.')) {
                        if (c == '.')
                            dotCounter++;
                        if (dotCounter > 1)
                            return null;
                        number += c;
                        if (i + 1 < expr.length() && expr.toCharArray()[i + 1] != ')')
                            c = expr.toCharArray()[++i];
                        else break;
                    }
                    if (i + 1 < expr.length() && expr.toCharArray()[i + 1] != ')')
                        --i;
                    operands.push(Double.parseDouble(number));
                    mayUnary = false;
                }
            }
        }
        while (!operators.empty())
            executeOperator(operands, operators.pop());

        if(operands.peek() == null)
            return null;

        return formatAnswer(operands.pop().toString());
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            return processExpression(statement);
        } catch (Exception e) {
            return null;
        }
    }

}
